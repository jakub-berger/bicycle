# Template for HTML sites

Project contains GULP and folder/file structure.

### Plugins:

1. [Autoprefixer](https://github.com/sindresorhus/gulp-autoprefixer#readme)
2. [Browser-Sync](https://www.browsersync.io/docs/gulp)
3. [Babel](https://www.npmjs.com/package/gulp-babel)
4. [Sourcemaps](https://www.npmjs.com/package/gulp-sourcemaps)
5. [Clean-CSS](https://www.npmjs.com/package/gulp-clean-css)
6. [Imagemin](https://www.npmjs.com/package/gulp-imagemin)
7. [SASS](https://www.npmjs.com/package/gulp-sass)
8. [Uglify](https://www.npmjs.com/package/gulp-uglify)
9. [Mode](https://www.npmjs.com/package/gulp-mode)

### Usage

1. Developmnent mode: **gulp watch --dev**
2. Production mode: **gulp watch --prod**
3. Image compression: **gulp compress**
4. Database: **json-server --watch database.json --port 3001**

**Image compression** is used as a separate process to maintain better controll of folders and files structure inside _dist/image_ directory.

**Database** To run database please install _JSON Server_ and go to the database folder. If you don't have _JSON Server_ inastalled on your computer, type _npm install -g json-server_ in the command line.

### SCSS structure:

1. SASS

- **main.scss** - imports from all files inside _partials_ folder

2. PARTIALS

- **base.scss** - basic styles for project
- **layout.scss** - imports from files inside _themes_ folder, contains sets of additional color variables
- **mixins.scss** - mixins
- **modules.scss** - imports from files inside _modules_ folder
- **reset.scss** - styles to reset browsers styling
- **variables.scss** - all projects variables
- **typography.scss** - fonts imports

3. MODULES

- **navigation.scss** - rules for sites main navigation
- **header.scss** - rules for sites header
- **main.scss** - imports from files inside _sections_ folder
- **footer.scss** - rules for sites footer

4. SECTIONS

- inside this folder are all sites sections
- each file should be imported to **main.scss** inside _modules_ folder

5. THEMES

- inside this folder are files containing all additional projects color schemes
