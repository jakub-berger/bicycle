"use strict";

document.addEventListener("DOMContentLoaded", function () {
  var body = document.querySelector("body");
  var navigation = body.querySelector(".navigation");
  var links = navigation.querySelectorAll("a");
  var form = body.querySelector(".newsletter__form");
  var newsletterInput = body.querySelector(".newsletter__input");
  var newsletterSubmit = body.querySelector(".newsletter__submit");

  var navPosition = function navPosition(event) {
    if (window.scrollY > 30) {
      navigation.classList.add("position");
    } else {
      navigation.classList.contains("position") ? navigation.classList.remove("position") : null;
    }
  };

  navPosition();
  window.addEventListener("scroll", navPosition);
  var mySwiper = new Swiper(".swiper-container", {
    direction: "horizontal",
    loop: true,
    grabCursor: true,
    autoplay: {
      delay: 3000
    },
    pagination: {
      el: ".swiper-pagination",
      type: "bullets",
      bulletElement: "span",
      clickable: true
    }
  }); //------------------------------------------------------------------------

  var sendNewsletter = function sendNewsletter(event) {
    event.preventDefault();
    var span = form.querySelector(".newsletter__alert") ? form.querySelector(".newsletter__alert") : form.querySelector(".newsletter__send");
    span ? span.remove() : null;
    var newElement = document.createElement("span");

    if (newsletterInput.value.length === 0 || !newsletterInput.value.includes("@") || !newsletterInput.value.includes(".")) {
      newElement.classList.add("newsletter__alert");

      if (newsletterInput.value.length === 0) {
        newElement.textContent = "Pole nie może być puste.";
      } else {
        newElement.textContent = "Adres email musi zawierać znak '@' i znak '.'";
      }

      newsletterInput.classList.add("newsletter__input--alert");
      newsletterSubmit.classList.add("newsletter__submit--alert");
      form.appendChild(newElement);
    } else {
      send(newsletterInput.value);
      newElement.classList.add("newsletter__send");
      newElement.textContent = "Twój adres email został poprawnie dodany do bazy mailingowej.";
      newsletterInput.classList.remove("newsletter__input--alert");
      newsletterSubmit.classList.remove("newsletter__submit--alert");
      form.appendChild(newElement);
    }
  };

  form.addEventListener("submit", sendNewsletter);

  function send(data) {
    var message = {
      email: data
    };
    fetch("http://localhost:3001/user-address", {
      method: "POST",
      headers: {
        "Content-type": "application/json; charset=UTF-8"
      },
      body: JSON.stringify(message)
    }).then(function (resp) {
      return resp.json();
    })["catch"](function (error) {
      return console.dir("Błąd: ", error);
    });
  } //------------------------------------------------------------------------


  var autoscroll = function autoscroll(event) {
    event.preventDefault();
    var address = event.currentTarget.getAttribute("href").substring(1);
    var element = document.querySelector("[id=".concat(address, "]"));
    var navHeight;
    var toScroll = element.offsetTop;
    var nav = document.querySelector(".navigation");

    if (nav.offsetHeight >= 56) {
      navHeight = 56;
    }

    window.scrollTo({
      top: toScroll - navHeight,
      behavior: "smooth"
    });
  }; // const autoscroll = event => {
  //   event.preventDefault();
  //   const address = event.currentTarget.getAttribute("href").substring(1);
  //   const element = document.querySelector(`[id=${address}]`);
  //   const toScroll = element.getBoundingClientRect().top;
  //   const distance = element.offsetTop;
  //   let navHeight;
  //   let heightToScroll = 0;
  //   const isNegative = toScroll < 0 ? true : false;
  //   const startPosition = Math.abs(toScroll - distance);
  //   address === "body" ? (navHeight = 0) : (navHeight = 56);
  //   if (startPosition != 0) {
  //     heightToScroll = startPosition;
  //     let interval = setInterval(() => {
  //       if (startPosition > distance) {
  //         // go back
  //         heightToScroll -= 10;
  //         heightToScroll <= distance ? clearInterval(interval) : null;
  //       } else {
  //         // forward
  //         heightToScroll += 10;
  //         heightToScroll >= distance ? clearInterval(interval) : null;
  //       }
  //       window.scrollTo(0, heightToScroll);
  //     });
  //   } else {
  //     if (distance) {
  //       heightToScroll = startPosition;
  //       let interval = setInterval(() => {
  //         heightToScroll >= distance ? clearInterval(interval) : null;
  //         heightToScroll += 10;
  //         window.scrollTo(0, heightToScroll);
  //       });
  //     }
  //   }
  // };


  links.forEach(function (link) {
    return link.addEventListener("click", autoscroll);
  });
});
//# sourceMappingURL=app.js.map
