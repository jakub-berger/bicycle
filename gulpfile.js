const gulp = require("gulp");

// plugins
const sass = require("gulp-sass");
const autoprefixer = require("gulp-autoprefixer");
const sourcemaps = require("gulp-sourcemaps");
const cleanCSS = require("gulp-clean-css");
const imagemin = require("gulp-imagemin");
const babel = require("gulp-babel");
const uglify = require("gulp-uglify");
const browserSync = require("browser-sync").create();
const mode = require("gulp-mode")({
  modes: ["prod", "dev"],
  default: "dev",
  verbose: false
});

sass.compiler = require("node-sass");

// Files paths
const files = {
  htmlSource: "./*.html",
  sassSource: "./src/sass/**/*.scss",
  sassDestination: "./dist/css",
  imagesSource: "./src/img/**",
  imagesDestination: "./dist/images",
  scriptSource: "./src/scripts/*.js",
  scriptDestionation: "./dist/scripts"
};

// Styles
function style() {
  return gulp
    .src(files.sassSource)
    .pipe(mode.dev(sourcemaps.init()))
    .pipe(
      sass({
        errLogToConsole: true,
        outputStyle: "expanded" // Options: nested, expanded, compact, compressed
      }).on("error", sass.logError)
    )
    .pipe(
      autoprefixer({
        flexbox: true,
        grid: "autoplace"
      })
    )
    .pipe(mode.prod(cleanCSS()))
    .pipe(mode.dev(sourcemaps.write("/")))
    .pipe(gulp.dest(files.sassDestination))
    .pipe(browserSync.stream());
}

// Images
function compressImages() {
  return gulp
    .src(files.imagesSource)
    .pipe(
      imagemin([
        imagemin.gifsicle({
          interlaced: true,
          optimizationLevel: 2 // Level: 1-3
        }),
        imagemin.jpegtran({
          progressive: true
        }),
        imagemin.optipng({
          optimizationLevel: 5 // Level: 1-7
        }),
        imagemin.svgo({
          plugins: [
            {
              removeViewBox: true
            },
            {
              cleanupIDs: false
            }
          ]
        })
      ])
    )
    .pipe(gulp.dest(files.imagesDestination));
}

//JavaScript
async function jsCompatibility() {
  return gulp
    .src(files.scriptSource)
    .pipe(mode.dev(sourcemaps.init()))
    .pipe(
      mode.dev(
        babel({
          presets: ["@babel/env"],
          inputSourceMap: true
        })
      )
    )
    .pipe(
      mode.prod(
        babel({
          presets: ["@babel/env"]
        })
      )
    )
    .pipe(mode.prod(uglify()))
    .pipe(mode.dev(sourcemaps.write(".")))
    .pipe(gulp.dest(files.scriptDestionation));
}

// Browser Sync
function watch() {
  browserSync.init({
    server: {
      baseDir: "./"
    }
  });
  gulp.watch(files.sassSource, style);
  gulp.watch(files.htmlSource).on("change", browserSync.reload);
  gulp.watch(files.scriptSource, jsCompatibility);
  gulp.watch(files.imagesSource, compressImages);
}

exports.style = style;
exports.compress = compressImages;
exports.js = jsCompatibility;
exports.watch = watch;
