document.addEventListener("DOMContentLoaded", () => {
  const body = document.querySelector("body");
  const navigation = body.querySelector(".navigation");
  const links = navigation.querySelectorAll("a");
  const form = body.querySelector(".newsletter__form");
  const newsletterInput = body.querySelector(".newsletter__input");
  const newsletterSubmit = body.querySelector(".newsletter__submit");

  const navPosition = event => {
    if (window.scrollY > 30) {
      navigation.classList.add("position");
    } else {
      navigation.classList.contains("position") ? navigation.classList.remove("position") : null;
    }
  };

  navPosition();
  window.addEventListener("scroll", navPosition);

  const mySwiper = new Swiper(".swiper-container", {
    direction: "horizontal",
    loop: true,
    grabCursor: true,
    autoplay: {
      delay: 3000
    },

    pagination: {
      el: ".swiper-pagination",
      type: "bullets",
      bulletElement: "span",
      clickable: true
    }
  });

  //------------------------------------------------------------------------

  const sendNewsletter = event => {
    event.preventDefault();

    const span = form.querySelector(".newsletter__alert") ? form.querySelector(".newsletter__alert") : form.querySelector(".newsletter__send");
    span ? span.remove() : null;

    let newElement = document.createElement("span");

    if (newsletterInput.value.length === 0 || !newsletterInput.value.includes("@") || !newsletterInput.value.includes(".")) {
      newElement.classList.add("newsletter__alert");

      if (newsletterInput.value.length === 0) {
        newElement.textContent = "Pole nie może być puste.";
      } else {
        newElement.textContent = "Adres email musi zawierać znak '@' i znak '.'";
      }

      newsletterInput.classList.add("newsletter__input--alert");
      newsletterSubmit.classList.add("newsletter__submit--alert");
      form.appendChild(newElement);
    } else {
      send(newsletterInput.value);
      newElement.classList.add("newsletter__send");
      newElement.textContent = "Twój adres email został poprawnie dodany do bazy mailingowej.";
      newsletterInput.classList.remove("newsletter__input--alert");
      newsletterSubmit.classList.remove("newsletter__submit--alert");
      form.appendChild(newElement);
    }
  };

  form.addEventListener("submit", sendNewsletter);

  function send(data) {
    const message = {
      email: data
    };

    fetch("http://localhost:3001/user-address", {
      method: "POST",
      headers: {
        "Content-type": "application/json; charset=UTF-8"
      },
      body: JSON.stringify(message)
    })
      .then(resp => resp.json())
      .catch(error => console.dir("Błąd: ", error));
  }

  //------------------------------------------------------------------------

  const autoscroll = event => {
    event.preventDefault();
    const address = event.currentTarget.getAttribute("href").substring(1);
    const element = document.querySelector(`[id=${address}]`);
    let navHeight;

    const toScroll = element.offsetTop;
    const nav = document.querySelector(".navigation");
    if (nav.offsetHeight >= 56) {
      navHeight = 56;
    }

    window.scrollTo({
      top: toScroll - navHeight,
      behavior: "smooth"
    });
  };

  // const autoscroll = event => {
  //   event.preventDefault();
  //   const address = event.currentTarget.getAttribute("href").substring(1);
  //   const element = document.querySelector(`[id=${address}]`);
  //   const toScroll = element.getBoundingClientRect().top;
  //   const distance = element.offsetTop;
  //   let navHeight;

  //   let heightToScroll = 0;
  //   const isNegative = toScroll < 0 ? true : false;
  //   const startPosition = Math.abs(toScroll - distance);

  //   address === "body" ? (navHeight = 0) : (navHeight = 56);

  //   if (startPosition != 0) {
  //     heightToScroll = startPosition;
  //     let interval = setInterval(() => {
  //       if (startPosition > distance) {
  //         // go back
  //         heightToScroll -= 10;
  //         heightToScroll <= distance ? clearInterval(interval) : null;
  //       } else {
  //         // forward
  //         heightToScroll += 10;
  //         heightToScroll >= distance ? clearInterval(interval) : null;
  //       }
  //       window.scrollTo(0, heightToScroll);
  //     });
  //   } else {
  //     if (distance) {
  //       heightToScroll = startPosition;
  //       let interval = setInterval(() => {
  //         heightToScroll >= distance ? clearInterval(interval) : null;
  //         heightToScroll += 10;
  //         window.scrollTo(0, heightToScroll);
  //       });
  //     }
  //   }
  // };

  links.forEach(link => link.addEventListener("click", autoscroll));
});
